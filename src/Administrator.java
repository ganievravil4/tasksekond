/*
 * Administrator
 *
 * Данный класс содержит информацию о администраторе и списке всех пользователей системы
 *
 * создатель класса Равиль Ганиев
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Administrator {


    private String fio;
    private static List<Administrator> listAdmin = new ArrayList<>();
    private static List<User> listUser = new ArrayList<>();

    public Administrator(String fio) {
        this.fio = fio;
        listAdmin.add(this);
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public static List<User> getListUser() {
        return listUser;
    }

    public static List<Administrator> getListAdmin() {
        return listAdmin;
    }

    public static void addListUser(User user) {
        listUser.add(user);
    }

    public void useSectionAdministrator(){
       Scanner scanner = new Scanner(System.in);
       String label = "";

       while (!label.equals("exit")) {
           System.out.println("Для просмотра списка пользователей введите 1" +
                             "\nДля возврата в предыдущее меню введите exit");
           label = scanner.next();

           switch(label) {
               case ("1"):
                   System.out.println("Список пользователей системы: " + "\n"
                                     + getListUser() + "\b");
               case ("exit"):
                   break;
               default:
                   System.out.println("Нажали чего то не то! Продолжите вводить корректные значения!");
                   break;
           }
       }
    }

    @Override
    public String toString() {
        return "Администратор: "+getFio();
    }
}
