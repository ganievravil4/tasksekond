import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static List<Administrator> listAdmins = new ArrayList<>();
    public static List<Teacher> listTeachers = new ArrayList<>();
    public static List<Student> listStudents = new ArrayList<>();
    public static List<Test> listTests = new ArrayList<>();
    public static List<Question> listQuestions = new ArrayList<>();

    static {
        Administrator admin = new Administrator("Ганиева Анастасия Юрьевна");
        Teacher prepod1 = new Teacher("Иванов Иван Иванович");
        Teacher prepod2 = new Teacher("Петров Петр Петрович");
        Test test1Prepod1 = new Test("Отечественная история", prepod1);
        Test test2Prepod1 = new Test("Зарубежная история", prepod1);
        Test test1Prepod2 = new Test("Отечественная литература", prepod2);
        Test test2Prepod2 = new Test("Зарубежная литература", prepod2);
        Student stud1 = new Student("Сидоров Сидор Сидорович", prepod1);
        Student stud2 = new Student("Алексеев Алексей Алексеевич", prepod2);
        Student stud3 = new Student("Борисов Борис Борисович", prepod1, prepod2);
        Student stud4 = new Student("Николаев Николай Николаевич", prepod1, prepod2);

        Question q1test1 = new Question(test1Prepod1, "В каком году была куликовская битва","1480","1380", "1280", "1580", "1380");
        Question q2test1 = new Question(test1Prepod1, "В каком году была бородинская битва","1480","1380", "1280", "1812", "1812");
        Question q3test1 = new Question(test1Prepod1, "В каком году была курская битва","1480","1380", "1943", "1580", "1943");
        Question q4test1 = new Question(test1Prepod1, "В каком году было стояние на реке угре","1480","1380", "1280", "1580", "1480");
        Question q1test2 = new Question(test2Prepod1, "Сколько лет шла столетняя война","100","10", "50", "200", "100");
        Question q2test2 = new Question(test2Prepod1, "Сколько лет просуществовала Византия","100","1000", "50", "200", "1000");
        Question q3test2 = new Question(test2Prepod1, "В каком году началась 1ая мировая","1001","910", "1914", "2003", "1914");
        Question q4test2 = new Question(test2Prepod1, "В каком году началась 2ая мировая","1001","910", "1914", "1939", "1939");

        Question q1test3 = new Question(test1Prepod2, "Как звали Пушкина","Искандэр","Александр", "Николай Васильевич", "Толстой", "Александр");
        Question q2test3 = new Question(test1Prepod2, "Как звали Лермонтова","Ицхак","лорд Байрон", "Ленин", "Михаил", "Михаил");
        Question q3test3 = new Question(test1Prepod2, "Роман Толстого: ","Один дома","Другой мир", "Война и мир", "Группа крови", "Война и мир");
        Question q4test3 = new Question(test1Prepod2, "Как звали Онегина","Евгений","Джордж", "Йода", "Остап Ибрагимович", "Евгений");
        Question q1test4 = new Question(test2Prepod2, "Кто придумал ктулху","Лавкрафт","Уве Бол", "Дарт Вейдер", "Соломон Кейн", "Лавкрафт");
        Question q2test4 = new Question(test2Prepod2, "Кто написал мартен иден","Мартин иден","Джек Лондон", "Том Такер", "Бендер", "Джек Лондон");
        Question q3test4 = new Question(test2Prepod2, "Кто написал тома сойера","1001","910", "Марк твен", "2003", "Марк твен");
        Question q4test4 = new Question(test2Prepod2, "из какой страны О. Генри","1001","910", "1914", "США", "США");
        Collections.addAll(listAdmins, admin);
        Collections.addAll(listTeachers, prepod1, prepod2);
        Collections.addAll(listStudents, stud1, stud2, stud3, stud4);
        Collections.addAll(listTests, test1Prepod1, test2Prepod1, test1Prepod2, test2Prepod2);
        Collections.addAll(listQuestions, q1test1, q2test1, q3test1, q4test1, q1test2, q2test2, q3test2, q4test2,
                          q1test3, q2test3, q3test3, q4test3, q1test4, q2test4, q3test4, q4test4);
    }

    public static void main(String[] args) {

        consoleMenu();

    }

        public static void consoleMenu() {
            Scanner scanner = new Scanner(System.in);
            String label = "";

            while (!label.equals("exit")) {
                System.out.println("Вы:\n" + "1.Администратор\n" + "2.Преподаватель\n" + "3.Студент\n" +
                                  "4.Для завершения работы введите exit" + "\nВведите от 1 до 4 для выбора.");
                try {
                    // int a = Integer.parseInt(reader.readLine());
                    label = scanner.next();
                    switch (label) {
                        case ("1"):
                            System.out.println("Вы:\n" + Administrator.getListAdmin());
                            System.out.println("Введите от 0 до " + (Administrator.getListAdmin().size() - 1) + " для выбора");
                            Administrator.getListAdmin().get(scanner.nextInt()).useSectionAdministrator();
                            break;
                        case ("2"):
                            System.out.println("Вы:\n");
                            for (User user : Administrator.getListUser()) {
                                if (user instanceof Teacher)
                                    System.out.println(user.toString() + " Введите: " + Administrator.getListUser().indexOf(user));
                            }
                            Administrator.getListUser().get(scanner.nextInt()).useSection();
                            break;
                        case ("3"):
                            System.out.println("Вы:\n");
                            for (User user : Administrator.getListUser()) {
                                if (user instanceof Student)
                                    System.out.println(user.toString() + " Введите: " + Administrator.getListUser().indexOf(user));
                            }
                            Administrator.getListUser().get(scanner.nextInt()).useSection();
                            break;
                        case ("exit"):
                            break;
                        default:
                            System.out.println("Нажали чего то не то! Начните заново!");
                            consoleMenu();
                            break;
                    }
                } catch (Exception e) {
                    System.out.println("Введите корректное значение!");
                    scanner.next();
                    consoleMenu();
                }
            }
        }
    }

