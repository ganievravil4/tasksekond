/*
 * Question
 *
 * Данный класс содержит информацию о вопросе для теста, информацию о тесте которому он принадлежит,
 * а так же реализует метод selestVariant() для выбора варианта ответа, определение его соответствию верному варианту
 * и передачи информации вызывающему его.
 *
 * создатель класса Равиль Ганиев
 */

public class Question {


    private String textQuestion;
    private Test ownerTest;
    private String variantA;
    private String variantB;
    private String variantC;
    private String variantD;
    private String trueVariant;

    public Question(Test test, String textQ, String varA, String varB,
                    String varC, String varD, String trueVariant) {
        ownerTest = test;
        textQuestion = textQ;
        variantA = varA;
        variantB = varB;
        variantC = varC;
        variantD = varD;
        this.trueVariant = trueVariant;
        ownerTest.getListQuestion().add(this);
    }

    public String getTextQuestion() {
        return textQuestion;
    }

    public void setTextQuestion(String text) {
        textQuestion = text;
    }

    public Test getOwnerTest() {
        return ownerTest;
    }

    public void setOwnerTest(Test test) {
        ownerTest = test;
    }

    public String getVariantA() {
        return variantA;
    }

    public void setVariantA(String variantA) {
        this.variantA = variantA;
    }

    public String getVariantB() {
        return variantB;
    }

    public void setVariantB(String variantB) {
        this.variantB = variantB;
    }

    public String getVariantC() {
        return variantC;
    }

    public void setVariantC(String variantC) {
        this.variantC = variantC;
    }

    public String getVariantD(){
        return variantD;
    }

    public void setVariantD(String variantD) {
        this.variantD = variantD;
    }

    public String getTrueVariant() {
        return trueVariant;
    }

    public void setTrueVariant(String trueVariant) {
        this.trueVariant = trueVariant;
    }

    public int selectVariant(int variantStudent) {
        int result = 0;

        switch (variantStudent) {
        case ( 1 ):
           result = getVariantA().equals( getTrueVariant() ) ? 1 : 0;
           break;
        case ( 2 ):
            result = getVariantB().equals( getTrueVariant() ) ? 1 : 0;
            break;
        case ( 3 ):
            result = getVariantD().equals( getTrueVariant() ) ? 1 : 0;
            break;
        case ( 4 ):
            result = getVariantC().equals( getTrueVariant() ) ? 1 : 0;
            break;
        default:
            System.out.println("Нажали чего то не то! Начните заново!");
            break;
        }

        return result;
    }

    @Override
    public String toString() {
        return  "\b\bВопрос № " + (getOwnerTest().getListQuestion().indexOf(this)+1)+"\n"
                + getTextQuestion()+"\n"
                + "1)"+getVariantA()+"\n"
                + "2)"+getVariantB()+"\n"
                + "3)"+getVariantC()+"\n"
                + "4)"+getVariantD()+"\n";
    }
}
