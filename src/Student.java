/*
 * Student
 *
 * Данный класс содержит информацию о студенте, списке тестов и списке преподавателей, и реализует метод answerTest()
 * позволяющий пройти тест и отправить результаты преподавателю.
 *
 * создатель класса Равиль Ганиев
 */
import java.util.*;

public class Student implements User{


    private String fio;
    private List<Test> listTests;
    private List<Teacher> listTeachers;

    public Student(String fio, Teacher...teachers) {
        this.fio = fio;
        listTeachers = Arrays.asList(teachers);
        listTests = new ArrayList<>();
        for(Teacher teacher : teachers) {
            listTests.addAll(teacher.getListTests());
            teacher.getListStudents().add(this);
        }
        Administrator.addListUser(this);
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public List<Test> getListTests() {
        return listTests;
    }

    public List<Teacher> getListTeacher() {
        return listTeachers;
    }

    public void useSection() {
        String label = "";
        Scanner scanner = new Scanner(System.in);

        while (!label.equals("exit")) {
            System.out.println("Введите 1 для просмотра своих тестов и возможности их выполнения.\n" +
                    "Введите exit для возврата в предыдущее меню.");
            label = scanner.next();

            switch (label) {
                case ("1"):
                    System.out.println(this.getListTests() + "\b");
                    System.out.println("Введите от 0 до " + (this.getListTests().size()-1));
                    this.answerTest(this.getListTests().get(scanner.nextInt()), scanner);
                    System.out.println("Нажмите любую клавишу для выхода в предыдущее меню.");
                    scanner.next();
                    break;
                case ("exit"):
                    break;
                default:
                    System.out.println("Нажали чего то не то! Продолжите вводить корректные значения!");
                    break;
            }
        }
        //scanner.close();
    }

    public void answerTest(Test test, Scanner scanner) {
        int result = 0;

            for (int i = 0; i < test.getListQuestion().size();i++) {
                System.out.println(test.getListQuestion().get(i) + "\n" + "Выберите верный вариант ответа:");

                if(scanner.hasNextInt()) {
                    result += test.getListQuestion().get(i).selectVariant(scanner.nextInt());
                } else {
                    i--;
                    scanner.next();
                }
            }

        System.out.println("Вы решили тест на " + (((double) result / 4) * 100) + " процентов.");
        test.getTeacherOwner().getStudentResult().add
        (new TestResult(this, test, (((double) result / 4) * 100)));
    }

    @Override
    public String toString() {
        return "\b\bСтудент: " + getFio() + "\n";
    }
}



