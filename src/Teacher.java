/*
 * Teacher
 *
 * Данный класс содержит информацию о преподавателе, студентах которые подписаны на него, тестах им созданных
 * а также о результатах решения теста студентами
 *
 * создатель класса Равиль Ганиев
 */
import java.util.*;

public class Teacher implements User {


    private String fio;
    private List<Test> listTests;
    private List<Student> listStudents;
    private Set<TestResult> studentResult;

    public Teacher(String fio) {
        this.fio = fio;
        listTests = new ArrayList<>();
        listStudents = new ArrayList<>();
        studentResult = new HashSet<>();
        Administrator.addListUser(this);
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public List<Test> getListTests() {
        return listTests;
    }

    public List<Student> getListStudents() {
        return listStudents;
    }

    public Set<TestResult> getStudentResult() {
        return studentResult;
    }

    public void useSection() {
        String label = "";
        Scanner scanner = new Scanner(System.in);

        while (!label.equals("exit")) {
            System.out.println("Введите 1 для просмотра своих тестов.\n" +
                               "Введите 2 для просмотра результатов тестирования студентов.\n" +
                               "Введите exit для возврата в предыдущее меню.");
            label = scanner.next();

            switch (label) {
                case ("1"):
                    System.out.println(this.getListTests() + "\b");
                    System.out.println("Введите от 0 до " + (this.getListTests().size()-1) +
                                       " для просмотра соответствующего теста.");
                    System.out.println(this.getListTests().get(scanner.nextInt()).getListQuestion() + "\b");
                    System.out.println("Нажмите любую клавишу для выхода в предыдущее меню.");
                    scanner.next();
                    break;
                case ("2"):
                    System.out.println("Результаты тестирований ваших студентов:\n" + this.getStudentResult() + "\b\b");
                    System.out.println("Нажмите любую клавишу для выхода в предыдущее меню.");
                    scanner.next();
                    break;
                case ("exit"):
                    break;
                default:
                    System.out.println("Нажали чего то не то! Продолжите вводить корректные значения!");
                    break;
            }
        }
        //scanner.close();
    }

    @Override
    public String toString() {
        return "\b\bПреподаватель: " +
                getFio()  + "\n";
    }
}
