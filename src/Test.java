/*
 * Test
 *
 * Данный класс содержит информацию о тесте, вопросах его наполняющих и преподавателе который его создал.
 * В конструкторе вносится в список теста преподавателя создателя.
 *
 * создатель класса Равиль Ганиев
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {


    private String nameTest;
    private List<Question> listQuestion;
    private Teacher teacherOwner;

    public Test(String nameTest, Teacher teacherOwner) {
        this.nameTest = nameTest;
        this.teacherOwner = teacherOwner;
        teacherOwner.getListTests().add(this);
        listQuestion = new ArrayList<>();
    }

    public String getNameTest() {
        return nameTest;
    }

    public void setNameTest(String nameTest) {
        this.nameTest = nameTest;
    }

    public List<Question> getListQuestion() {
        return listQuestion;
    }

    public Teacher getTeacherOwner() {
        return teacherOwner;
    }

    @Override
    public String toString() {
        return "\b\bТест: " + getNameTest()+"\n"
                + "количество вопросов: " + getListQuestion().size() + "\n";
    }
}
