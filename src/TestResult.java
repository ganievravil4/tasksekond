/*
 * TestResult
 *
 * Данный класс содержит информацию о результатах теста определенного студента.
 *
 * создатель класса Равиль Ганиев
 */
public class TestResult {


    private Student student;
    private Test test;
    private double procentOfCompletion;

    public TestResult(Student student, Test test, double procentOfCompletion) {
        this.student = student;
        this.test = test;
        this.procentOfCompletion = procentOfCompletion;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public double getProcentOfCompletion() {
        return procentOfCompletion;
    }

    public void setProcentOfCompletion(double procentOfCompletion) {
        this.procentOfCompletion = procentOfCompletion;
    }

    @Override
    public String toString() {
        return  "\b\b" + getStudent() +
                " выполнил : " + getTest() +
                " на - " + getProcentOfCompletion() + " процентов.";
    }
}
